from selenium import webdriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service
from selenium.webdriver.common.by import By
import pandas as pd
from datetime import datetime

service = Service(executable_path=EdgeChromiumDriverManager().install())
driver = webdriver.Edge(service = service)

page = 1
items=[]
stock_dic={'大量現貨': 'High', '少量存貨': 'Low', '請先查詢': 'Enquiry'}
time_format='%Y-%m-%d'

while True:
    url = f'https://www.price.com.hk/product.php?p=525136&page={page}'
    driver.get(url)
    shops = driver.find_elements(By.CSS_SELECTOR,"ul.list-unstyled.list-inline div.item-inner")
    for shop in shops:
        s_name=shop.find_element(By.CSS_SELECTOR,"p.quotation-merchant-name > a").text
        s_address=shop.find_element(By.CSS_SELECTOR,"p.quotation-merchant-address > a").text
        s_rating=shop.find_element(By.CSS_SELECTOR,"div.quotation-merchant-spec-rating b").text
        if s_rating=='-':
            s_rating=None
        else:
            s_rating=float(s_rating)
        s_stock=stock_dic[shop.find_element(By.CSS_SELECTOR,"div.quote-source div[class^=quote-stock-status-]").text]
        s_update=datetime.strptime(shop.find_element(By.CSS_SELECTOR,"div.quote-source > div").text,time_format).date()
        all_prices=shop.find_elements(By.CSS_SELECTOR,"div.quote-price-normal > div")
        s_price={}
        for price in all_prices:
            s_price[price.get_attribute('class')]= \
                float(price.find_element(By.CSS_SELECTOR,"span.text-price-number.fx").get_attribute('data-price'))
        items.append([s_name, s_address, s_rating, s_stock, s_update, s_price.get('quote-price-hong'), s_price.get('quote-price-water')])
    if 'disabled' in driver.find_element(By.CSS_SELECTOR,'ul.pagination > li.next-btn').get_attribute('class'):
        break
    page+=1

df = pd.DataFrame(items, columns=['Shop Name', 'Address', 'Rating', 'Stock', 'Update Date', 'Dealer Good', 'Grey Import'])
print(df.head())
driver.close()
